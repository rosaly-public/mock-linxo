<?php

declare(strict_types=1);

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

// Following composer autoloader path resolver is picked up from PHPUnit source code.
if (isset($GLOBALS['_composer_autoload_path'])) {
    define('COMPOSER_INSTALL', $GLOBALS['_composer_autoload_path']);
    unset($GLOBALS['_composer_autoload_path']);
} else {
    foreach (array(__DIR__ . '/../../autoload.php', __DIR__ . '/../vendor/autoload.php', __DIR__ . '/vendor/autoload.php') as $file) {
        if (file_exists($file)) {
            define('COMPOSER_INSTALL', $file);
            break;
        }
    }
    unset($file);
}

if (!defined('COMPOSER_INSTALL')) {
    fwrite(
        STDERR,
        'You need to set up the project dependencies using Composer:' . PHP_EOL . PHP_EOL .
        '    composer install' . PHP_EOL . PHP_EOL .
        'You can learn all about Composer on https://getcomposer.org/.' . PHP_EOL
    );

    exit(1);
}

require COMPOSER_INSTALL;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    public function registerBundles(): array
    {
        return [new Symfony\Bundle\FrameworkBundle\FrameworkBundle()];
    }

    #[Route(methods: ['GET'], path: '/')]
    public function index(RouterInterface $router): JsonResponse
    {
        $routes = [];

        foreach ($router->getRouteCollection()->all() as $route) {
            $routes[] = implode('|', $route->getMethods()) . ' ' . $route->getPath();
        }

        return new JsonResponse(['routes' => $routes]);
    }

    #[Route(methods: ['POST'], path: '/token')]
    public function auth(): JsonResponse
    {
        return new JsonResponse([
            'access_token' => sha1('access_token'),
            'refresh_token' => sha1('refresh_token'),
            'expires_in' => (new \DateTimeImmutable('1 year'))->getTimestamp(),
        ]);
    }

    #[Route(methods: ['GET'], path: '/v2.1/accounts/{accountId}')]
    public function getAccount(Request $request, string $accountId): JsonResponse
    {
        $balance = $request->query->get('wanted_balance') ?? random_int(1000, 25000) / 10;

        return new JsonResponse([
            'account_id' => $accountId,
            'balance' => (string) $balance,
            'balance_date' => time(),
        ]);
    }
}

$kernel = new Kernel('dev', true);
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
