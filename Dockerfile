FROM php:8.1
# Needed by Symfony-CLI
RUN apt update && apt install -y git

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN curl -sS https://get.symfony.com/cli/installer | bash
RUN mv /root/.symfony5/bin/symfony /usr/local/bin/symfony

WORKDIR /var/www/mock-linxo
COPY . .
RUN composer install --optimize-autoloader

EXPOSE 8000
CMD ["/usr/local/bin/symfony", "server:start"]
