# Mock Linxo

## Getting started

```
docker build -t mock-linxo .
docker run -p 8000:8000 -it mock-linxo
```

Go to <http://127.0.0.1:8000/>

## Installation as a dependency

In the `composer.json`, add:

```json
"repositories": [
    {
        "type": "git",
        "url": "https://gitlab.com/rosaly-public/mock-linxo.git"
    }
]
```

Then:

```
composer require --dev "rosaly-public/mock-linxo:dev-master"
```

Add the following script:

```
"mock:linxo-server": "php -S 0.0.0.0:8888 vendor/rosaly-public/mock-linxo/index.php"
```
